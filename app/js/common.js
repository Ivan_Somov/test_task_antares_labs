$( document ).ready(function(){


  $(function() {
    $(".hiddener, .main_article").addClass("display_none");
    $(".hidden_nav").addClass(".display_none");
  });



  $(function(){
      var $w = $(window);
      var $circ = $('.animated-circle');
      var $progCount = $('.progress-count');
      var wh = $w.height();
      var h = $('body').height();
      var sHeight = h - wh;
      $w.on('scroll', function(){
          var perc = Math.max(0, Math.min(1, $w.scrollTop()/sHeight));
          updateProgress(perc);
      });

      function updateProgress(perc){
          var circle_offset = 126 * perc;
          $circ.css({
              "stroke-dashoffset" : 126 - circle_offset
          });
          $progCount.html(Math.round(perc * 100) + "%");
      }

  }());

  $(".progress_bar_a").click(function() {
    $(".popup").removeClass("display_none");
    $(".hiddener, .main_article").addClass("display_none");
  });

  $(".button").click(function() {
    $(".popup").addClass("display_none");
    $(".hiddener, .main_article").removeClass("display_none");
  });

  $(window).scroll(function() {
    if ($(document).scrollTop() > 75) {
      $(".hidden_nav_first").addClass("display_none");
      $(".hidden_nav").removeClass("display_none");
      $(".hidden_nav").addClass("navigation");
      $(".logo_image").addClass("display_none_header");
      $(".special_class").addClass("add_mergin_top");
    } else {

      $(".hidden_nav").addClass("display_none");
      $(".hidden_nav_first").removeClass("display_none");
      $(".hidden_nav").removeClass("navigation");
      $(".logo_image").removeClass("display_none_header");
      $(".special_class").removeClass("add_mergin_top");
    }
  });


});
